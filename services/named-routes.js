(function (angular) {
    'use strict';


    /*
        Use ng-switch to perform deep routing

        ng-click="goto('video', {video: id})"

        /videos/:videoID/edit

        <!-- Include SubView Content. -->
        .when(/videos/:videoID/:tagID
        <div ng-switch="videoID != undefined">
            <div ng-switch-when="true" ng-include=" ... "></div>
            <div ng-switch-when="false" ng-include=" ... "></div>
            <div ng-switch-default ng-include=" ... ">unused at the moment</div>
        </div>
    */


    angular.module('Core')

        .provider('$namedRoute', ['$routeProvider', function ($routeProvider) {
            var namedRoutes = {},
                paramMatcher = /(?:\:)(.*?)(?:(\/|$)\/?)/gm;

            this.when = function (path, route) {
                if (route.name) {
                    var altPath = path,
                        matches = altPath.match(paramMatcher) || [],
                        param,
                        i;

                    // this converts paths '/:param1/:param2' to '/{{param1}}/{{param2}}/'
                    for (i = 0; param = matches[i]; i += 1) {  // this will break on undefined
                        altPath = altPath.replace(param, '{{' + param.replace(/\:|\//gm, '') + '}}/');
                    }

                    namedRoutes[route.name] = altPath.slice(0, -1);
                }

                // Update the actual route provider
                $routeProvider.when(path, route);

                return this;
            };

            this.otherwise = $routeProvider.otherwise;

            this.$get = ['$interpolate', '$route', '$routeParams', '$location', function ($interpolate, $route, $routeParams, $location) {
                var result;

                // Create an interpolation function for the routes
                angular.forEach(namedRoutes, function (route, name) {
                    result = $interpolate(route, true);
                    if (!!result) {
                        namedRoutes[name] = result;
                    }
                });

                // Update the route object with named routes
                $route.to = {};
                $route.pathTo = {};

                // For all the named routes, allow automatic param injection with override based on the existing context
                angular.forEach(namedRoutes, function (route, name) {
                    if (typeof route === 'function') {
                        $route.to[name] = function (params) {
                            params = angular.extend({}, $routeParams, params);
                            $location.path(route(params));
                        };
                        $route.pathTo[name] = function (params) {
                            params = angular.extend({}, $routeParams, params);
                            return route(params);
                        };
                    } else {
                        $route.to[name] = function () {
                            $location.path(route);
                        };
                        $route.pathTo[name] = route;
                    }
                });

                // We just return the original route service
                return $route;
            }];
        }])
        .run(['$namedRoute', function () {
            // inject $namedRoute into $route
        }]);

}(this.angular));
